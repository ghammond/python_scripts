# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 17:01:51 2020

@author: ande583
"""

import os
import sys
import argparse
from openfile_function import open_file

    

parcer = argparse.ArgumentParser(description='Finding strings in files')

parcer.add_argument('-p','--path',type=str,default='.',help='the path to the folder you want to search')

parcer.add_argument('-f','--filename',default = 'none',type=str,help='name of the data file')

parcer.add_argument('-s','--string',default='none',type=str,help='the string you want to find')

parcer.add_argument('-e','--extension',action='store',default='.txt',type=str,help='file extension of files you want to search')

parcer.add_argument('-b','-bb','--batch',type=str,help='something')


args = parcer.parse_args()
filename = args.filename
path = args.path

open_file(path,filename)
