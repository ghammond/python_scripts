
import random

class Person:
    def __init__(self,name,gender,age,race):
        self.name = name
        self.gender = gender
        self.age = age
        self.race = race

    def SetAge(self,age):
        self.age = age

    def SetGender(self,gender):
        self.gender = gender

    def SetRace(self,race):
        self.race = race

    def PrintInfo(self):
        return print('Name: ',self.name,' Gender: ',self.gender,' Age: ',self.age,' Race: ',self.race)

    def PrintAge(self):
        return print(self.name,' is ',self.age)

    def PrintGender(self):
        return print(self.name,' is ',self.gender)

    def PrintRace(self):
        return print(self.name,' is ',self.race)

    def IsMale(self):
        if self.gender == 'male' or self.gender == 'Male' or self.gender == 'M' or self.gender == 'm':
            return print(True)
        else:
            return print(False)

    def IsAdult(self):
        if self.age >= 18:
            return print(True)
        else:
            return print(False)

class Firefighter(Person):

    def __init__(self,name,gender,age,race,training_exp_date,rank,station):
        super().__init__(name,gender,age,race)
        self.training_exp_date = training_exp_date
        self.rank = rank
        self.station = station


    def PrintInfo(self):
        super().PrintInfo()
        print('Training Expiration Date: ',self.training_exp_date,' Rank: ',self.rank,' Station: ',self.station)
        return 

    def PrintRank(self):
        return print(self.name,' is a ',self.rank)

class Student(Person):

    def __init__(self,name,gender,age,race,grade,gpa,major,employment):
        super().__init__(name,gender,age,race)
        self.grade = grade
        self.gpa = gpa
        self.major = major
        self.employment = employment
        

    def PrintInfo(self):
        return print('Name: ',self.name,' Gender: ',self.gender,' Age: ',self.age,' Race: ',self.race,' Grade: ',self.grade,' GPA: ',self.gpa,' Major: ',self.major,' Employed: ',self.employment)

    def PrintGpa(self):
        return print(self.name,' has a ',self.gpa,' GPA')

    def PrintMajor(self):
        return print(self.name,' is a ',self.major,' major')

    def IsEmployed(self):
        if self.employment == True:
            return print(True)
        else:
            return print(False)

John = Firefighter('John','Male',20,'African American','September 23rd, 2020','Captain','New York City')
John.PrintInfo()
John.PrintAge()
John.PrintRace()
print(John.name,John.age,John.gender,John.race)
John.IsMale()
John.IsAdult()
John.PrintRank()
Karen = Student('Karen','Female',35,'Hispanic',12,3.97,'Geology',True)
Karen.PrintGpa()
Karen.PrintMajor()
Karen.PrintInfo()
Karen.IsEmployed()
Karen.IsMale()
Karen.IsAdult()
Karen.PrintRace()
Karen.PrintGender()











