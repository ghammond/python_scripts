# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 13:04:53 2020

@author: ande583
"""


import matplotlib.pyplot as plt

class Plotter(object):
    def __init__(self, xval=None, yval=None,y1val=None,y2val=None,title=None,title1=None,title2=None,title3=None):
        self.xval = xval
        self.yval = yval
        self.y1val = y1val
        self.y2val = y2val
        self.title = title
        self.title1 = title1
        self.title2 = title2
        self.title3 = title3

    def plotgraph(self):
        f = plt.figure()
        sp = f.add_subplot(111)
        sp.plot(self.xval, self.yval, 'o-')
        plt.xlabel(self.title)
        plt.ylabel(self.title1)
        plt.title(self.title1)
        return f
    
    def two_subplots(self):
        fig,ax = plt.subplots(2)
        ax[0].plot(self.xval,self.yval,'o-')
        ax[0].set_xlabel(self.title)
        ax[0].set_ylabel(self.title1)
        ax[0].title.set_text(self.title1)
        ax[1].plot(self.xval,self.y1val,'o-')
        ax[1].title.set_text(self.title2)
        ax[1].set_xlabel(self.title)
        ax[1].set_ylabel(self.title2)
        return fig
    
    def three_subplots(self):
        fig,ax = plt.subplots(3)
        ax[0].plot(self.xval,self.yval,'o-')
        ax[0].set_xlabel(self.title)
        ax[0].set_ylabel(self.title1)
        ax[0].title.set_text(self.title1)
        ax[1].plot(self.xval,self.y1val,'o-')
        ax[1].title.set_text(self.title2)
        ax[1].set_xlabel(self.title)
        ax[1].set_ylabel(self.title2)
        ax[2].plot(self.xval,self.y2val,'o-')
        ax[2].set_xlabel(self.title)
        ax[2].set_ylabel(self.title3)
        ax[2].title.set_text(self.title3)
        return fig
        