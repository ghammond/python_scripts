# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 15:02:06 2020

@author: ande583
"""

import matplotlib.pyplot as plt
import tkinter as tk
from tkinter import *
from gui_openfile_function import open_file
from tkinter import filedialog
from tkinter import messagebox
import numpy as np
from class_plotter import Plotter


window = tk.Tk()


window.title('Plotting Data')
tbox1 = tk.Label(text='File path:')
tbox1.grid(row=0,column=0)
window.geometry('650x250+500+500')

e = Entry(window,width=50,borderwidth=3)
e.focus_set()
e.grid(row=0,column=1,columnspan=3)

menubar = Menu(window) 
  
# Adding File Menu and commands 
file = Menu(menubar, tearoff = 0) 
menubar.add_cascade(label ='File', menu = file) 
file.add_command(label ='New File', command = None) 
file.add_command(label ='Open...', command = None) 
file.add_command(label ='Save', command = None) 
file.add_command(label ='Save as',command = None)
file.add_command(label='Print',command = None)
file.add_separator() 
file.add_command(label ='Exit', command = window.destroy) 
  
# Adding Edit Menu and commands 
edit = Menu(menubar, tearoff = 0) 
menubar.add_cascade(label ='Edit', menu = edit) 
edit.add_command(label ='Cut', command = None) 
edit.add_command(label ='Copy', command = None) 
edit.add_command(label ='Paste', command = None) 
edit.add_command(label ='Select All', command = None) 
edit.add_separator() 
edit.add_command(label ='Find...', command = None) 
edit.add_command(label ='Find again', command = None) 
  
# Adding Help Menu 
help_ = Menu(menubar, tearoff = 0) 
menubar.add_cascade(label ='Help', menu = help_) 
help_.add_command(label ='Tk Help', command = None) 
help_.add_command(label ='Gui explanation', command = None) 
help_.add_separator() 
help_.add_command(label ='Guide to tkinter', command = None) 
  
# display Menu 
window.config(menu = menubar) 


def browse():
    window.filename = filedialog.askopenfilename(initialdir='/home/jacob/Downloads/plotting',title='Select A File',filetypes=(('All Files','*.*'),('tec files','*.tec'),('dat files','*.dat'),('mat files','*.mat'),('text files','*.txt')))
    e.delete(0,tk.END)
    e.insert(0,window.filename)
    
    
    
    global drop_down
    global my_menu
    global drop_down2
    global menu
    global menu1
    
    
    full_filename = e.get()

    f = open_file(full_filename,'r')
    global list
    list = f.readlines()
    f.close()

    
    options = list[0]
    for entry in options:
        opt = options.strip()
        opt2 = opt.split(',')

    final_opt = []
    for entry in opt2:
        string = entry.lstrip('"').rstrip('"')
        string1 = string.split(' m')
        final_opt.append(string1[0])

    final_opt1 = [] 
    for entry in final_opt:
        final_string = entry.split(' w')
        final_opt1.append(final_string)
        
    global var
    global var1
    global var2
    global var3
    global var4
    global new_list
    global num
    
    var = StringVar()
    var.set(final_opt[0])
    
    var1 = StringVar()
    var1.set(final_opt[1])
    
    var2 = IntVar()
    var2.set(0)
    
    var3 = StringVar()
    var3.set(final_opt[2])
    
    var4 = StringVar()
    var4.set(final_opt[3])
    
    new_list = []
    
    for i in range(len(opt2)):
        new_list.append([i+1,final_opt1[i][0]])
        num = final_opt
        
    
    drop_down = OptionMenu(window,var,*num)
    my_menu = OptionMenu(window,var1,*num)
    drop_down2 = OptionMenu(window,var2,1,2,3,command=clicked)
    menu = OptionMenu(window,var3,*num) 
    menu1 = OptionMenu(window,var4,*num) 
    
    drop_down2.grid(row=3,column=4)
    drop_down2_label = tk.Label(text='Number of graphs')
    drop_down2_label.grid(row=2,column=4)
    
    plotting.grid(row=4,column=2)
    
    #drop_down.grid_forget()
    #menu.grid_forget()
    #menu1.grid_forget()
    #my_menu.grid_forget()
    
def closewindow():
    window.destroy()
    
def clicked(event):
    global q 
    q = IntVar()
    q = var2.get()
    if q == 1:
        drop_down.grid_forget()
        my_menu.grid_forget()
        menu.grid_forget()
        menu1.grid_forget()
        
        drop_down.grid(row=3,column=0)
        drop_down_label = tk.Label(text='x-axis')
        drop_down_label.grid(row=2,column=0)
    
        my_menu.grid(row=3,column=1)
        my_menu_label = tk.Label(text='1st y-axis')
        my_menu_label.grid(row=2,column=1)
        
    elif q == 2:
        drop_down.grid_forget()
        my_menu.grid_forget()
        menu.grid_forget()
        menu1.grid_forget()
        
        drop_down.grid(row=3,column=0)
        drop_down_label = tk.Label(text='x-axis')
        drop_down_label.grid(row=2,column=0)
    
        my_menu.grid(row=3,column=1)
        my_menu_label = tk.Label(text='1st y-axis')
        my_menu_label.grid(row=2,column=1)
        
        menu.grid(row=3,column=2)
        menu_label = Label(text='2nd y-axis')
        menu_label.grid(row=2,column=2)
        
    elif q == 3:
        drop_down.grid_forget()
        my_menu.grid_forget()
        menu.grid_forget()
        menu1.grid_forget()
        
        drop_down.grid(row=3,column=0)
        drop_down_label = tk.Label(text='x-axis')
        drop_down_label.grid(row=2,column=0)
    
        my_menu.grid(row=3,column=1)
        my_menu_label = tk.Label(text='1st y-axis')
        my_menu_label.grid(row=2,column=1)
        
        menu.grid(row=3,column=2)
        menu_label = Label(text='2nd y-axis')
        menu_label.grid(row=2,column=2)
        
        menu1.grid(row=3,column=3)
        menu1_label = Label(text='3rd y-axis')
        menu1_label.grid(row=2,column=3) 

       
def plot():
    global xaxis
    global yaxis
    global y1axis
    global y2axis
    xaxis = StringVar()
    yaxis = StringVar()
    y1axis = StringVar()
    y2axis = StringVar()
    
    del list[0]

    list2 = []
    for entry in list:
        s1 = entry.strip()
        s2 = s1.rstrip('\n')
        s3 = s2.split(',')
        list2.append(s3)
    
    list3 = []
    for i in range(len(list2)):
        for j in range(len(list2[i])):
            str1 = list2[i][j]
            str2 = str1.split()
            list3.append(str2)
        
# Convert values to floats
    
    list4 = []        
    for i in range(len(list3)):
        for j in range(len(list3[i])):
            str3 = list3[i][j]
            list4.append(float(str3))          
    
# Calculate the column number        
    global col_num
    col_num = IntVar()
    col_num = len(list3[0])
    
    global list_arg
    global list_arg1
    global list_arg2
    global list_arg3
    global title
    global title1
    global title2
    global title3
    
    if q == 1:
        
        xaxis = var.get()
        yaxis = var1.get()
        for i in range(len(new_list)):    
            if xaxis == new_list[i][1]:
                title = new_list[i][1]
                list_arg = list4[i::col_num]
   
        for i in range(len(new_list)):
            if yaxis == new_list[i][1]:
                title1 = new_list[i][1]
                list_arg1 = list4[i::col_num]
                
        x = np.array(list_arg)
        y = np.array(list_arg1)
        
        
        graph = Plotter(x,y,None,None,title,title1)
        graph.plotgraph()
        plt.tight_layout()
        plt.show()
    
    elif q == 2:
        
        xaxis = var.get()
        yaxis = var1.get()
        y1axis = var3.get()
        for i in range(len(new_list)):    
            if xaxis == new_list[i][1]:
                title = new_list[i][1]
                list_arg = list4[i::col_num]
   
        for i in range(len(new_list)):
            if yaxis == new_list[i][1]:
                title1 = new_list[i][1]
                list_arg1 = list4[i::col_num]
    
        for i in range(len(new_list)):
            if y1axis == new_list[i][1]:
                title2 = new_list[i][1]
                list_arg2 = list4[i::col_num]
                
        x = np.array(list_arg)
        y = np.array(list_arg1)
        y1 = np.array(list_arg2)
        
        
        graph = Plotter(x,y,y1,None,title,title1,title2)
        graph.two_subplots()
        plt.tight_layout()
        plt.show()
            
    elif q == 3:
        
        xaxis = var.get()
        yaxis = var1.get()
        y1axis = var3.get()
        y2axis = var4.get()
        
        for i in range(len(new_list)):    
            if xaxis == new_list[i][1]:
                title = new_list[i][1]
                list_arg = list4[i::col_num]
   
        for i in range(len(new_list)):
            if yaxis == new_list[i][1]:
                title1 = new_list[i][1]
                list_arg1 = list4[i::col_num]
    
        for i in range(len(new_list)):
            if y1axis == new_list[i][1]:
                title2 = new_list[i][1]
                list_arg2 = list4[i::col_num]
            
        for i in range(len(new_list)):
            if y2axis == new_list[i][1]:
                title3 = new_list[i][1]
                list_arg3 = list4[i::col_num]
                
        x = np.array(list_arg)
        y = np.array(list_arg1)
        y1 = np.array(list_arg2)
        y2 = np.array(list_arg3)
        
        
        graph = Plotter(x,y,y1,y2,title,title1,title2,title3)
        graph.three_subplots()
        plt.tight_layout()
        plt.show()
    
    
    
file_browse = Button(window,text='Find file',padx=20,command=browse)  
close = Button(window,text='Close',padx=20,command=closewindow)
plotting = Button(window,text='Plot',padx=33,command=plot)


file_browse.grid(row=0,column=4)
close.grid(row=11,column=4)



window.mainloop()







