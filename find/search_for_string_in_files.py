# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 17:01:51 2020

@author: ande583
"""

import os
import sys
import argparse
    

parcer = argparse.ArgumentParser(description='Finding strings in files')

parcer.add_argument('-p','--path',type=str,default='.',help='the path to the folder you want to search')

parcer.add_argument('-s','--string',default='none',type=str,help='the string you want to find')

parcer.add_argument('-e','--extension',action='store',default='.txt',type=str,help='file extension of files you want to search')

args = parcer.parse_args()

extension = args.extension
path = args.path
string = args.string

if args.string == 'none':
    while args.string == 'none':
        s = input('Enter a string:\n')
        break
    if s == '':
        while s == '':
            s = input('Enter a string:\n') 
            if not s == '': 
                string = s
                break
    else:
        string = s

if args.path == '.':
    while args.path == '.':
        p = input('Input alternate path:\n')
        break
    if p == '' or p == 'no':
        print('Code will run with default path')
        q = input('Is that ok? (yes/no)\n')
        q = str(q).lower()
        if q == 'yes':
            path = args.path
        if q == 'no':
            while q == 'no':
                p = input('Input alternate path:\n')
                if os.path.exists(p):
                    path = p
                    break 
                else:
                    continue 
    else:
        if os.path.exists(p):
            path = p
        else:
            continue

if args.extension == '.txt':      
    while args.extension == '.txt':
        e = input('Input alternate extension:\n')
        break
    if e == '' or e == 'no':
        print('Default extension will be used')
        q = input('Proceed? (yes/no, use custom extension)\n')
        q = q.lower()
        if q == 'yes' or q == '':
            extension = args.extension
        if q == 'no':
            while q == 'no':
                e = input('Input alternate extension:\n')
                if not e == '':
                    extension = e 
                    break
    else:         
        extension = e


if not os.path.isdir(path):
    print('The path does not exist')

count = 0
for root, dirnames, filenames in os.walk(path):
    for filename in filenames:
        if filename.endswith(extension):
            full_filename = os.path.join(root,filename)
            with open(full_filename) as f:
                if string in f.read():
                    count += 1
                else:
                    print(filename)
                    
if count == 1:
    print(string,' appears in',count,'filename')
else:
    print(string,' appears in',count,'filenames')                      
            
